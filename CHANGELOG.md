
## 1.0.0

  * GHC 8.4 compatibility.
  * Drop support for GHC < 8.0.

